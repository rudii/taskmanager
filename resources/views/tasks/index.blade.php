<h1 style="display: inline-block;">Tasks</h1>
@auth
<p style="display: inline-block;"><a href="/profile">{{auth()->user()->name}}</a></p>
@endauth

<div>
    <form action="/tasks" method="get" id="priority" style="display: inline-block;">
        <select name="priority" id="priorityDropdown" onchange="document.getElementById('priority').submit()">
            <option value="">- Priority -</option>
            <option value="low">Low</option>
            <option value="medium">Medium</option>
            <option value="high">High</option>
        </select>
    </form>
    <form action="/tasks" method="get" id="status" style="display: inline-block;">
        <select name="status" id="statusDropdown" onchange="document.getElementById('status').submit()">
            <option>- Status -</option>
            <option value="not started">Not Started</option>
            <option value="in progress">In Progress...</option>
            <option value="done">Done</option>
        </select>
    </form>
    <form action="/tasks" method="get" style="display: inline-block;">
        <input type="text" name="search" id="search" placeholder="Search...">
        <input type="button" value="Search">
        </form>
</div>


<script>
    const urlParams = new URLSearchParams(location.search);

    for (const [key, value] of urlParams) {
        console.log(`${key}:${value}`);
        if (key == "status") {
            document.getElementById("statusDropdown").value = value
        }
        if (key == "priority") {
            document.getElementById("priorityDropdown").value = value
        }
        if (key == "search") {
            document.getElementById("search").value = value
        }
    }
</script>


@unless(count($tasks) == 0)
@foreach($tasks as $task)
<h2><a href="/tasks/{{$task['id']}}">{{$task['title']}}</a></h2>
<h5>{{$task['priority']}} - {{$task['status']}}</h5>
<p>{{$task['description']}}</p>
<hr>
@endforeach

@else
    <p>No tasks</p>
@endunless
<br><br><br>
@auth
    <a href="/tasks/create">Create Task</a>
@else
    <a href="/login">Login</a>
@endauth