<h1 style="display: inline-block;">Edit Task</h1>
@auth
<p style="display: inline-block;"><a href="/profile">{{auth()->user()->name}}</a></p>
@endauth



<form action="/tasks/{{$task['id']}}" method="post">
    @csrf
    @method('PUT')
    <table>
        <tr>
            <td style="text-align: right">
                Title @error('title')is required @enderror
            </td>
            <td>
                <input type="text" name="title" value="{{$task['title']}}">
            </td>
        </tr>
        <tr>
            <td style="text-align: right">
                Description @error('description')is required @enderror
            </td>
            <td>
                <textarea name="description" cols="30" rows="10">{{$task['description']}}</textarea>
            </td>
        </tr>
        <tr>
            <td style="text-align: right">
                Priority @error('priority')is required @enderror
            </td>
            <td>
                <select name="priority" id="prior">
                    <option value="low">low</option>
                    <option value="medium">medium</option>
                    <option value="high">high</option>
                </select>
            </td>
        </tr>
        <tr>
            <td style="text-align: right">
                Status @error('status')is required @enderror
            </td>
            <td>
                <select name="status" id="stat">
                    <option value="not started">not started</option>
                    <option value="in progress">in progress...</option>
                    <option value="done">done</option>
                </select>
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <button type="submit">Submit</button> 
            </td>
        </tr>    
    </form>
        
        <tr>
            <td></td>
            <td>
                <form action="/tasks/{{$task['id']}}" method="post" id="delform">
                    @csrf
                    @method('DELETE')
                    <button ondblclick='del();' type="reset" title="Double-click to delete">Delete</button>
                </form>
            </td>
        </tr>
    </table>


<script>
    let select = document.getElementById("prior");
    select.value = "{{$task['priority']}}";
    let stat = document.getElementById("stat");
    stat.value = "{{$task['status']}}";
    function del(){
        document.getElementById("delform").submit()
    }
</script>