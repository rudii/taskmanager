<h1 style="display: inline-block;">New Task</h1>
@auth
<p style="display: inline-block;"><a href="/profile">{{auth()->user()->name}}</a></p>
@endauth



<form action="/tasks" method="post">
@csrf
<table>
    <tr>
        <td style="text-align: right">
            Title @error('title')is required @enderror
        </td>
        <td>
            <input type="text" name="title" value="{{old('title')}}">
        </td>
    </tr>
    <tr>
        <td style="text-align: right">
            Description @error('description')is required @enderror
        </td>
        <td>
            <textarea name="description" cols="30" rows="10">{{old('description')}}</textarea>
        </td>
    </tr>
    <tr>
        <td style="text-align: right">
            Priority @error('priority')is required @enderror
        </td>
        <td>
            <select name="priority" value="{{old('priority')}}">
                <option value=""><i></i></option>
                <option value="low">low</option>
                <option value="medium">medium</option>
                <option value="high">high</option>
            </select>
        </td>
    </tr>
    <tr>
        <td></td>
        <td>
            <button type="submit">Submit</button>
        </td>
    </tr>


</table>
</form>
