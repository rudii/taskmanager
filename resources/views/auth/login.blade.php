<h1>Tasks</h1>
<h2>Login</h2>


<form action="/login" method="post">
    @csrf
    @if (isset($err))
        <p>We could not authenticate you</p>
    @endif
    <table>
        <tr>
            <td style="text-align: right">
                @error('email') How am I supposted to find you, when you do not give me an @enderror Email
            </td>
            <td>
                <input type="email" name="email" value="{{old("email")}}">
            </td>
        </tr>
        <tr>
            <td style="text-align: right">
                @error('password') I believe, that you have a @enderror Password
            </td>
            <td>
                <input type="password" name="password">
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <button type="submit">Login</button> 
            </td>
        </tr>    
    </table>
</form>
