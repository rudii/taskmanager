<h1>Tasks</h1>
<h2>Registering</h2>


<form action="/register" method="post">
    @csrf
    <table>
        <tr>
            <td style="text-align: right">
                @error('name') You have to enter @enderror Name
            </td>
            <td>
                <input type="text" name="name" value="{{old("name")}}">
            </td>
        </tr>
        <tr>
            <td style="text-align: right">
                @error('email') You have to enter @enderror Email
            </td>
            <td>
                <input type="email" name="email" value="{{old("email")}}">
            </td>
        </tr>
        <tr>
            <td style="text-align: right">
                Password @error('password')is required and it does not have to be the same kind of passwords you are most possible to forget. 123 is good enough :) @enderror
            </td>
            <td>
                <input type="password" name="password">
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <button type="submit">Register</button> 
            </td>
        </tr>    
    </table>
</form>
