<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tasks extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'description', 'priority', "status"];

    public function scopeFilter($query, array $filter) {
        if($filter['status'] ?? false) {
            $query->where("status", "like", request('status'));
        };

        if($filter['priority'] ?? false) {
            $query->where("priority", "like", request('priority'));
        };

        if($filter['search'] ?? false) {
            $query->where("title", "like", "%". request('search'). "%")
                ->orWhere("description", "like", "%". request('search'). "%");
        };
    }
}
