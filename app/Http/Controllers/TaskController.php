<?php

namespace App\Http\Controllers;

use App\Models\tasks;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class TaskController extends Controller
{
    public function index() {
        // dd($request);
        return view('tasks.index', [
            'tasks' => tasks::latest()->filter(request(["search", "status", "priority"]))->get(),
            'query' => request(["search", "status", "priority"])
        ]);
    }

    public function show(tasks $task) {
        return view('tasks.show', [
            'task' => $task
        ]);
    }

    public function create() {
        return view('tasks.create');
    }

    public function store(Request $request) {
        $formFields = $request->validate([
            "title" => "required",
            "description" => "required",
            "priority" => Rule::in("low", "medium", "high"),
            "status" => "required"
        ]);

        tasks::create($formFields);

        return redirect("/tasks");
    }

    public function edit(tasks $task) {
        return view('tasks.edit', [
            'task' => $task
        ]);
    }

    public function update(Request $request, tasks $task) {
        $formFields = $request->validate([
            "title" => "required",
            "description" => "required",
            "priority" => Rule::in("low", "medium", "high"),
            "status" => "required"
        ]);

        $task->update($formFields);

        return redirect("/tasks");
    }

    public function delete(tasks $task) {
        $task->delete();
        return redirect("/tasks");
    }
}
