<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;


class AuthController extends Controller
{
    public function create() {
        return view('auth.create');
    }

    public function store(Request $request) {
        $formFields = $request->validate([
            "name" => "required",
            "email" => ["email", "required", Rule::unique("users","email")],
            "password" => "required",
        ]);

        $user = User::create($formFields);

        auth()->login($user);

        return redirect("/");
    }

    public function loginPage () {
        return view('auth.login');
    }

    public function loginForm (Request $request) {
        $formFields = $request->validate([
            "email" => ["email", "required"],
            "password" => "required",
        ]);

        if(auth()->attempt($formFields)) {
            $request->session()->regenerate();
            return redirect("/");
        }
        return view('auth.login', ['err'=>401]);
        
    }

    public function logout (Request $request) {
        auth()->logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect('/');
    }

    public function profile () {
        return view('auth.profile', ['user'=>auth()->user()]);
    }
}
